﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PhysiXEngine;

namespace Test
{
    static class ModelExtraction
    {
        public static Vector3 GetPosition(this Model model, String boneName)
        {
            return model.Bones[boneName].Transform.Translation;
        }

        public static Vector3 GetSize(this Model model, String boneName)
        {
            return model.GetPosition(boneName + "_size") - model.GetPosition(boneName);
        }

    }

    class BasketBallTest : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;

        #region "testing components"
        Crate baseColumn;
        Crate horColumn;
        Crate Board;
        Crate[] ring = new Crate[3];
        List<Ball> balls = new List<Ball>();
        List<Crate> crates = new List<Crate>();
        Gravity g = new Gravity(Vector3.Down * 10);
        ContactGenerator cg = new ContactGenerator();
        Camera camera;
        Panel panel;
        bool tClicked = false;
        bool Changed = false;
        #endregion

        SpriteFont arial;
        bool gamePaused = false;
        public BasketBallTest()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        private void GetValue()
        {
            if (!panel.Show && Changed)
            {
                ContactGenerator.friction = panel.GetVlaue("Friction");
                ContactGenerator.restitution = panel.GetVlaue("Restitution");
                g.gravity = new Vector3(
                    panel.GetVlaue("gevX"), panel.GetVlaue("gevY"), panel.GetVlaue("gevZ")
                    );
                Changed = false;
            }
        }

        void CreatePanel()
        {
            Initialize();
            panel.AddField("Friction", ContactGenerator.friction);
            panel.AddField("Restitution", ContactGenerator.restitution);
            panel.AddLabel("Gravity", "Gravity");
            panel.AddXYZ(g.gravity, "gev");
            panel.AddOkButton();
            panel.AddCancelButton();
            panel.Show = true;
        }

        BoundingBox underBasket;
        BoundingBox aboveBasket;

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        protected override void Initialize()
        {
            cg.positionEpsilon = 0.01f;
            baseColumn = new Crate(new Vector3(0.2f, 1.5f, 0.2f));
            baseColumn.Lock();
            crates.Add(baseColumn);

            horColumn = new Crate(new Vector3(0.2f,0.2f,0.4f));
            horColumn.Position = new Vector3(0, baseColumn.HalfSize.Y, baseColumn.HalfSize.Z);
            horColumn.Lock();
            crates.Add(horColumn);

            Board = new Crate(new Vector3(0.5f,0.5f,0.05f));
            Board.Position = horColumn.Position + new Vector3(0,horColumn.HalfSize.Y,horColumn.HalfSize.Z) ;
            Board.Lock();
            crates.Add(Board);

            ring[0] = new Crate(new Vector3(0.05f,0.05f,0.4f));
            ring[0].Position = Board.Position + Vector3.Left * 0.2f + Vector3.Backward * 0.12f + Vector3.Down * 0.15f;
            ring[0].Lock();
            crates.Add(ring[0]);

            ring[1] = new Crate(new Vector3(0.05f, 0.05f, 0.4f));
            ring[1].Position = Board.Position + Vector3.Right * 0.2f + Vector3.Backward * 0.12f + Vector3.Down * 0.15f;
            ring[1].Lock();
            crates.Add(ring[1]);

            ring[2] = new Crate(new Vector3(0.15f,0.05f,0.05f));
            ring[2].Position = (ring[0].Position + ring[1].Position) / 2f + Vector3.Backward * 0.35f;
            ring[2].Lock();
            crates.Add(ring[2]);

            underBasket = new BoundingBox(
                ring[0].Position - ring[0].HalfSize - Vector3.Down*0.4f,
                ring[1].Position + ring[1].HalfSize - Vector3.Down*0.4f);
            aboveBasket = new BoundingBox(
                ring[0].Position - ring[0].HalfSize,
                ring[1].Position + ring[1].HalfSize );
            

            foreach (Crate crate in crates)
            {
                cg.AddBody(crate);
            }
            camera = new Camera(this, new Vector3(0, 0, 0.1f), Vector3.Zero, Vector3.Up);
            Components.Add(camera);

            panel = new Panel(this, new Vector2(Window.ClientBounds.Width / 2 - 200 / 2,
                Window.ClientBounds.Height / 2 - 150 / 2), 220, 150);
            Components.Add(panel);
            base.Initialize();
        }

        Model ballModel;
        Texture2D basketMap;
        Texture2D grass;

        protected override void LoadContent()
        {
            ballModel = Content.Load<Model>(@"ball");
            basketMap = Content.Load<Texture2D>(@"basket_map");
            grass = Content.Load<Texture2D>(@"grass");
            arial = Content.Load<SpriteFont>(@"GUI/Arial");
            Crate ground = new Crate(new Vector3(20f, 0.4f, 20));
            ground.Position = -baseColumn.HalfSize.Y * Vector3.UnitY;
            ground.Texture = grass;
            ground.Lock();
            crates.Add(ground);
            cg.AddBody(ground);

            foreach (Crate crate in crates)
            {
                crate.LoadContent(Content);
                if (ring.Contains(crate)) crate.Texture = grass;
                if (crate != ground && !ring.Contains(crate)) crate.Texture = null;
            }
            base.LoadContent();
        }

        float mouseValue = 0;
        bool firstIntersect = false;
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.T))
                tClicked = true;
            if (Keyboard.GetState().IsKeyUp(Keys.T) && tClicked)
            {
                CreatePanel();
                Changed = true;
                tClicked = false;
                GetValue();
                gamePaused = panel.Show;
            }

            if (gamePaused) return;

            if (balls.Count > 0)
            {
                if (
                aboveBasket.Contains(balls.Last().GetBoundingSphere()) == ContainmentType.Contains ||
                aboveBasket.Contains(balls.Last().GetBoundingSphere()) == ContainmentType.Intersects
                )
                {
                    firstIntersect = true;
                    //score++;
                }

                if ((
                underBasket.Contains(balls.Last().GetBoundingSphere()) == ContainmentType.Contains ||
                underBasket.Contains(balls.Last().GetBoundingSphere()) == ContainmentType.Intersects) &&
                firstIntersect
                )
                {
                    score++;
                    firstIntersect = false;
                }
            }
            

            

            float duration = gameTime.ElapsedGameTime.Milliseconds / 1000f;
            g.Update(duration);
            foreach (Crate crate in crates)
            {
                crate.Update(duration);
            }
            foreach (Ball ball in balls)
            {
                ball.Update(duration);
            }

            if ((Mouse.GetState().LeftButton == ButtonState.Pressed))
                mouseValue++;

            if ((Mouse.GetState().LeftButton == ButtonState.Released) && (mouseValue != 0)) 
            {
                Ball ball = createBall();
                ball.Position = camera.cameraPosition;
                ball.AddForce(camera.cameraDirection * (mouseValue * 100 + 1000));
                cg.AddBody(ball);
                balls.Add(ball);
                mouseValue = 0;
            }
            cg.Update(duration);

          

            base.Update(gameTime);
        }

        Ball createBall()
        {
            balls.Clear();
            Ball ball = new Ball(0.1f);
            ball.model = ballModel;
            ball.Texture = basketMap;
            ball.Mass = 2;
            g.AddBody(ball);
            return ball;
        }
        SpriteBatch sb;
        int score;
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            
            foreach (Crate crate in crates)
            {
                crate.Draw(camera);
            }

            foreach (Ball ball in balls)
            {
                ball.Draw(camera);
            }
         
            base.Draw(gameTime);
            sb = new SpriteBatch(GraphicsDevice);
            sb.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.AnisotropicWrap, DepthStencilState.Default, RasterizerState.CullCounterClockwise);
            sb.DrawString(arial, "Score :" + score +" pt", Vector2.One * 3, Color.Red);
            sb.End();
        }
    }
}
