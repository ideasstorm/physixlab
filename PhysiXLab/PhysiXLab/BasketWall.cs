﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PhysiXEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Test
{
    class BasketWall : DrawableGameComponent
    {
        public BasketWall(Game game) : base(game)
        {
            //this.Game = game;
        }

        Model basket;
        Camera camera;
        protected override void LoadContent()
        {
            basket = Game.Content.Load<Model>(@"basket");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Matrix[] transforms = new Matrix[basket.Bones.Count];
            basket.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in basket.Meshes)
            {
                foreach (BasicEffect be in mesh.Effects)
                {
                    be.EnableDefaultLighting();
                    be.World = mesh.ParentBone.Transform;
                    be.View = camera.view;
                    be.Projection = camera.projection;
                }
                mesh.Draw();
                base.Draw(gameTime);
            }
        }
    }
}
